From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import coq_tactics reduction.
From iris.proofmode Require Export tactics.
From sandbox.lang Require Export tactics lifting.
From sandbox.lang Require Import notation.
From iris.program_logic Require Import lifting.
Set Default Proof Using "Type".
Import uPred.

Lemma tac_wp_value `{sandboxG Σ} Δ E Φ e v s:
  language.IntoVal e v →
  envs_entails Δ (Φ v) → envs_entails Δ (WP e @ s; E {{ Φ }}).
Proof. rewrite envs_entails_eq=> ? ->. by apply wp_value. Qed.

Ltac wp_value_head := eapply tac_wp_value; [iSolveTC|reduction.pm_prettify].

Lemma tac_wp_pure `{sandboxG Σ} K Δ Δ' E e1 e2 φ n Φ p1 p2 s :
  PureExec φ n e1 p1 e2 p2 →
  φ →
  MaybeIntoLaterNEnvs n Δ Δ' →
  envs_entails Δ' (WP fill K (e2 at p2) @ s; E{{ Φ }}) →
  envs_entails Δ  (WP fill K (e1 at p1) @ s; E{{ Φ }}).
Proof.
  rewrite envs_entails_eq /PureExec => ??? HΔ'. rewrite into_laterN_env_sound /=.
  rewrite -wp_bind HΔ' -wp_pure_step_later //. by rewrite -wp_bind_inv.
Qed.

Tactic Notation "wp_pure" open_constr(efoc) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) => reshape_expr e ltac:(fun K e' =>
    unify e' efoc;
    eapply (tac_wp_pure K);
    [simpl; iSolveTC                (* PureExec *)
    |try done                       (* The pure condition for PureExec *)
    |iSolveTC                       (* IntoLaters *)
    |simpl_subst; try wp_value_head (* new goal *)])
   || fail "wp_pure: cannot find" efoc "in" e "or" efoc "is not a reduct"
  | _ => fail "wp_pure: not a 'wp'"
  end.

Tactic Notation "wp_rec" := wp_pure (App _ _).
Tactic Notation "wp_lam" := wp_rec.
Tactic Notation "wp_let" := wp_lam.
Tactic Notation "wp_seq" := wp_let.
Tactic Notation "wp_op" := wp_pure (BinOp _ _ _).
Tactic Notation "wp_if" := wp_pure (If _ _ _).
Tactic Notation "wp_case" := wp_pure (Case _ _); try wp_value_head.

Lemma tac_wp_bind `{sandboxG Σ} K Δ E Φ e s p:
  envs_entails Δ (WP (e at p) @ s; E {{ v, WP fill K (tstate_of_val v) @ s; E {{ Φ }} }})%I →
  envs_entails Δ (WP fill K (e at p) @ s; E {{ Φ }}).
Proof. rewrite envs_entails_eq=> ->. apply: wp_bind. Qed.

Ltac wp_bind_core K :=
  lazymatch eval hnf in K with
  | [] => idtac
  | _ => apply (tac_wp_bind K); simpl
  end.

Tactic Notation "wp_bind" open_constr(efoc) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) => reshape_expr e ltac:(fun K e' =>
    match e' with
    | efoc => unify e' efoc; wp_bind_core K
    end) || fail "wp_bind: cannot find" efoc "in" e
  | _ => fail "wp_bind: not a 'wp'"
  end.

(** tactics for working with the high heap *)
Section heap_high.
Context `{sandboxG Σ}.
Implicit Types P Q : iProp Σ.
Implicit Types Φ : tstate_val → iProp Σ.
Implicit Types Δ : envs (uPredI (iResUR Σ)).

Lemma tac_wp_alloc_high K Δ Δ' E j1 j2 (n : Z) Φ:
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  (∀ l (sz: nat), n = sz → ∃ Δ'',
    envs_app false (Esnoc (Esnoc Enil j1 (l ↦∗ replicate sz (LitV LitPoison))) j2 (†l…sz)) Δ'
      = Some Δ'' ∧
    envs_entails Δ'' (WP fill K (Lit $ LitLoc l at High) @ E ?{{ Φ }})) →
  envs_entails Δ (WP fill K (Alloc High (Lit $ LitInt n) at High) @ E ?{{ Φ }}).
Proof.
  rewrite envs_entails_eq=> ? HΔ. rewrite -wp_bind.
  eapply wand_apply; first exact:wp_alloc_high.
  rewrite -persistent_and_sep. apply and_intro; first by auto.
  rewrite into_laterN_env_sound; apply later_mono, forall_intro=> l.
  apply forall_intro=>sz. apply wand_intro_l. rewrite -assoc.
  rewrite sep_and. apply pure_elim_l=> Hn. apply wand_elim_r'.
  destruct (HΔ l sz) as (Δ''&?&HΔ'); first done.
  rewrite envs_app_sound //; simpl. by rewrite right_id HΔ'.
Qed.

Lemma tac_wp_free_high K Δ Δ' Δ'' Δ''' E i2 i3 vl (n : Z) (n' : nat) l Φ s:
  n = length vl →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i2 Δ' = Some (false, l ↦∗ vl)%I →
  envs_delete false i2 false Δ' = Δ'' →
  envs_lookup i3 Δ'' = Some (false, †l…n')%I →
  envs_delete false i3 false Δ'' = Δ''' →
  n' = length vl →
  envs_entails Δ''' (WP fill K (Lit LitPoison at High) @ s; E {{ Φ }}) →
  envs_entails Δ (WP fill K (Free (Lit $ LitInt n) (Lit $ LitLoc l) at High) @ s; E {{ Φ }}).
Proof.
  rewrite envs_entails_eq; intros -> ?? <- ? <- -> HΔ. rewrite -wp_bind.
  eapply wand_apply; first exact:wp_free_high; simpl.
  rewrite into_laterN_env_sound -!later_sep; apply later_mono.
  do 2 (rewrite envs_lookup_sound //). by rewrite HΔ True_emp emp_wand -assoc.
Qed.

Lemma tac_wp_read_high K Δ Δ' E i2 l q v Φ s p:
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i2 Δ' = Some (false, l ↦{q} v)%I →
  envs_entails Δ' (WP fill K (of_val v at p) @ s; E {{ Φ }}) →
  envs_entails Δ (WP fill K (Read (Lit $ LitLoc l) at p) @ s; E {{ Φ }}).
Proof.
  rewrite envs_entails_eq; intros ???.
  rewrite -wp_bind. eapply wand_apply; first exact:wp_read_high.
  rewrite into_laterN_env_sound -later_sep envs_lookup_split //; simpl.
    by apply later_mono, sep_mono_r, wand_mono.
Qed.

Lemma tac_wp_write_high K Δ Δ' Δ'' E i2 l v e v' Φ s:
  IntoVal e v' → surface_val v' →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i2 Δ' = Some (false, l ↦ v)%I →
  envs_simple_replace i2 false (Esnoc Enil i2 (l ↦ v')) Δ' = Some Δ'' →
  envs_entails Δ'' (WP fill K (Lit LitPoison at High) @ s; E {{ Φ }}) →
  envs_entails Δ (WP fill K (Write (Lit $ LitLoc l) e at High) @ s; E {{ Φ }}).
Proof.
  rewrite envs_entails_eq; intros ??????.
  rewrite -wp_bind. eapply wand_apply; first by apply wp_write_high.
  rewrite into_laterN_env_sound -later_sep envs_simple_replace_sound //; simpl.
  rewrite right_id. by apply later_mono, sep_mono_r, wand_mono.
Qed.
End heap_high.

(** Tactics for working with the low heap *)
Section heap_low.
Context `{sandboxG Σ}.
Implicit Types P Q : iProp Σ.
Implicit Types Φ : tstate_val → iProp Σ.
Implicit Types Δ : envs (uPredI (iResUR Σ)).

Lemma tac_wp_alloc_low K Δ Δ' E n Φ p:
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  (∀ l, low l →
    envs_entails Δ' (WP fill K (Lit $ LitLoc l at p) @ E ?{{ Φ }})) →
  envs_entails Δ (WP fill K (Alloc Low (Lit $ LitInt n) at p) @ E ?{{ Φ }}).
Proof.
  rewrite envs_entails_eq=> ? HΔ. rewrite -wp_bind.
  eapply wand_apply; first exact:wp_alloc_low.
  rewrite -persistent_and_sep.
  rewrite left_id into_laterN_env_sound; apply later_mono, forall_intro=> l.
  apply wand_intro_l. rewrite sep_and. apply pure_elim_l=> Hlow.
  by apply: HΔ.
Qed.

Lemma tac_wp_free_low K Δ Δ' E (n : Z) l Φ p:
  low l →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_entails Δ' (WP fill K (Lit LitPoison at p) @ E ? {{ Φ }}) →
  envs_entails Δ (WP fill K (Free (Lit $ LitInt n) (Lit $ LitLoc l) at p) @ E ? {{ Φ }}).
Proof.
  rewrite envs_entails_eq => ???. rewrite -wp_bind.
  eapply wand_apply; first exact:wp_free_low; simpl.
  rewrite -persistent_and_sep.
  by rewrite !left_id into_laterN_env_sound; apply later_mono.
Qed.

Lemma tac_wp_read_low K Δ Δ' E l Φ p:
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  (∀ v, surface_val v → envs_entails Δ' (WP fill K (of_val v at p) @ E ? {{ Φ }})) →
  envs_entails Δ (WP fill K (Read (Lit $ LitLoc l) at p) @ E ? {{ Φ }}).
Proof.
  rewrite envs_entails_eq => ? HΔ.
  rewrite -wp_bind. eapply wand_apply; first exact:wp_read_low.
  rewrite left_id into_laterN_env_sound; simpl.
  apply later_mono, forall_intro => v. apply wand_intro_l.
  rewrite sep_and. apply pure_elim_l=> Hlow.
  by apply: HΔ.
Qed.

Lemma tac_wp_write_low K Δ Δ' E l e v Φ p:
  IntoVal e v → low l → surface_val v →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_entails Δ' (WP fill K (Lit LitPoison at p) @ E ? {{ Φ }}) →
  envs_entails Δ (WP fill K (Write (Lit $ LitLoc l) e at p) @ E ? {{ Φ }}).
Proof.
  rewrite envs_entails_eq; move => ?????.
  rewrite -wp_bind. eapply wand_apply; first exact: wp_write_low.
  rewrite left_id into_laterN_env_sound left_id. by apply later_mono.
Qed.
End heap_low.


Tactic Notation "wp_apply" open_constr(lem) :=
  iPoseProofCore lem as false (fun H =>
    lazymatch goal with
    | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) =>
      reshape_expr e ltac:(fun K e' =>
        wp_bind_core K; iApplyHyp H; try iNext; simpl) ||
      lazymatch iTypeOf H with
      | Some (_,?P) => fail "wp_apply: cannot apply" P
      end
    | _ => fail "wp_apply: not a 'wp'"
    end).

Tactic Notation "wp_alloc_high" ident(l) "as" constr(H) constr(Hf) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_alloc_high K _ _ _ H Hf))
      |fail 1 "wp_alloc_high: cannot find 'Alloc' in" e];
    [iSolveTC
    |let sz := fresh "sz" in let Hsz := fresh "Hsz" in
     first [intros l sz Hsz | fail 1 "wp_alloc_high:" l "not fresh"];
     (* If Hsz is "constant Z = nat", change that to an equation on nat and
        potentially substitute away the sz. *)
     try (match goal with Hsz : ?x = _ |- _ => rewrite <-(Z2Nat.id x) in Hsz; last done end;
          apply Nat2Z.inj in Hsz;
          try (cbv [Z.to_nat Pos.to_nat] in Hsz;
               simpl in Hsz;
               (* Substitute only if we have a literal nat. *)
               match goal with Hsz : S _ = _ |- _ => subst sz end));
      eexists; split;
        [pm_reflexivity || fail "wp_alloc_high:" H "or" Hf "not fresh"
        |simpl; try wp_value_head]]
  | _ => fail "wp_alloc_high: not a 'wp'"
  end.

Tactic Notation "wp_alloc_high" ident(l) :=
  let H := iFresh in let Hf := iFresh in wp_alloc_high l as H Hf.

Tactic Notation "wp_free_high" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_free_high K))
      |fail 1 "wp_free: cannot find 'Free' in" e];
    [try fast_done
    |iSolveTC
    |let l := match goal with |- _ = Some (_, (?l ↦∗ _)%I) => l end in
     iAssumptionCore || fail "wp_free: cannot find" l "↦∗ ?"
    |pm_reflexivity
    |let l := match goal with |- _ = Some (_, († ?l … _)%I) => l end in
     iAssumptionCore || fail "wp_free: cannot find †" l "… ?"
    |pm_reflexivity
    |try fast_done
    |simpl; try first [wp_pure (Seq (Lit LitPoison) _)|wp_value_head]]
  | _ => fail "wp_free: not a 'wp'"
  end.

Tactic Notation "wp_read_high" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_read_high K))
      |fail 1 "wp_read: cannot find 'Read' in" e];
    [iSolveTC
    |let l := match goal with |- _ = Some ((_, ?l ↦{_} _))%I => l end in
     iAssumptionCore || fail "wp_read: cannot find" l "↦ ?"
    |simpl; try wp_value_head]
  | _ => fail "wp_read: not a 'wp'"
  end.

Tactic Notation "wp_write_high" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_write_high K); [iSolveTC|..])
      |fail 1 "wp_write: cannot find 'Write' in" e];
    [try done
    |iSolveTC
    |let l := match goal with |- _ = Some ((_, ?l ↦{_} _)%I) => l end in
     iAssumptionCore || fail "wp_write: cannot find" l "↦ ?"
    |pm_reflexivity
    |simpl; try first [wp_pure (Seq (Lit LitPoison) _)|wp_value_head]]
  | _ => fail "wp_write: not a 'wp'"
  end.

Section test_high.
Context `{sandboxG Σ}.
Example test_high : (WP (let: "l" := Alloc High #1 in
    "l" <- #0;;
    let: "v" := !"l" in
    Free #1 "l";;
    "v") at High ?{{v, True}})%I.
Proof.
  wp_alloc_high l as "Hl" "Hlfree". rewrite heap_mapsto_vec_singleton.
  wp_let.
  wp_write_high.
  wp_read_high.
  wp_let.
  rewrite -heap_mapsto_vec_singleton.
  wp_free_high => //.
Abort.
End test_high.

Tactic Notation "wp_alloc_low" ident(l) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_alloc_low K))
      |fail 1 "wp_alloc_low: cannot find 'Alloc' in" e];
    [iSolveTC
    | first [intros l | fail 1 "wp_alloc_low:" l "not fresh"]; simpl; try wp_value_head
    ]
  | _ => fail "wp_alloc_low: not a 'wp'"
  end.

Tactic Notation "wp_free_low" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp MaybeStuck ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_free_low K))
      |fail 1 "wp_free: cannot find 'Free' in" e];
    [try fast_done
    |iSolveTC
    |simpl; try first [wp_pure (Seq (Lit LitPoison) _)|wp_value_head]]
  | _ => fail "wp_free: not a 'wp' or not MaybeStuck"
  end.

Tactic Notation "wp_read_low" ident(v) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp MaybeStuck ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_read_low K))
      |fail 1 "wp_read: cannot find 'Read' in" e];
    [iSolveTC
    |simpl=>v; try wp_value_head]
  | _ => fail "wp_read: not a 'wp' or not MaybeStuck"
  end.

Tactic Notation "wp_write_low" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp MaybeStuck ?E (?e at ?p) ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' => eapply (tac_wp_write_low K); [iSolveTC|..])
      |fail 1 "wp_write_low: cannot find 'Write' in" e];
    [try fast_done
    |try fast_done
    |iSolveTC
    |simpl; try first [wp_pure (Seq (Lit LitPoison) _)|wp_value_head]]
  | _ => fail "wp_write_low: not a 'wp' or not MaybeStuck"
  end.
Section test_low.
Context `{sandboxG Σ}.
Example test_low : (WP (let: "l" := Alloc Low #1 in
    "l" <- #0;;
    let: "v" := !"l" in
    "l" <- "v";;
    Free #1 "l";;
    "v") at Low ?{{v, True}})%I.
Proof.
  wp_alloc_low l => ?.
  wp_let.
  wp_write_low.
  wp_read_low v => ?.
  wp_let.
  wp_write_low.
  wp_free_low => //.
Abort.
End test_low.
