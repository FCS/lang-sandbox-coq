From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type.
Set Default Proof Using "Type".

Definition assert : val :=
  λ: ["b"], if: "b" then #☠ else Syscall "abort" #1.

Section specs.
  Context `{sandboxG Σ}.

  Lemma type_assert p:
    assert ◁ fn p [# const_int 1] any.
  Proof.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl). inv_vec xl => b /=.
    iIntros "[Hb _]". simpl_subst.
    iApply (type_case with "[Hb]"). by iApply type_val.
    iIntros (i er v Heq) "Hv1 Hv2 _".
    move: i Heq => [|[|?]] /=;rewrite ?lookup_nil // => [[<-]].
    - iDestruct (const_int_inv with "Hv1") as %->.
      by iDestruct (const_int_inv with "Hv2") as %?.
    - by iApply type_val.
  Qed.

End specs.
