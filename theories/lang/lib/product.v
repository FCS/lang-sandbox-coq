From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type macro_helper.
Set Default Proof Using "Type".

Section product.
  Context `{sandboxG Σ}.

  Program Definition new_tuple n : val :=
    (@RecV <> (BNamed <$> gen_binders n) (
      let: "v" := Alloc High #n in
      gen_seq (imap (λ n s, ("v" +ₗ #n) <- Var s) (gen_binders n))
              ("v")) _).
  Next Obligation.
    move => ?. rewrite /Closed /=.
    split_and?; eauto using is_closed_of_val.
    apply gen_seq_closed => // ?. set_unfold. rewrite /Closed.
    move => [? [? [->]]] /=. intros ?%elem_of_list_lookup_2.
    case_bool_decide; set_solver.
  Qed.

  Local Hint Resolve gen_binders_NoDup.
  Lemma type_new_tuple n (tys : vec _ n) :
    new_tuple n ◁ fn High tys (product tys).
  Proof.
    iApply type_fn => //.
    { by rewrite fmap_length gen_binders_length. }
    { solve_surface_expr. apply: gen_seq_surface_expr => //. set_solver. }
    do 2 iModIntro. iIntros (xl) "Hxl".
    rewrite -(subst_env_empty (subst_v _ _ _)) -subst_v_subst_env //.
    rewrite subst_env_let subst_env_closed.
    iApply type_let; first by iApply type_new.
    iIntros (p) "Hp _". rewrite -subst_subst_env_l; last by apply lookup_delete.
    rewrite insert_delete gen_seq_subst_env fmap_imap subst_env_expr lookup_insert /= Nat2Z.id right_id.
    iApply (type_gen_seq (λ n,
      p ◁ product (take n tys ++ replicate (length tys - n) any))
              with "[Hp] [Hxl]").
    - by rewrite -minus_n_O vec_to_list_length.
    - rewrite big_sepL_imap /=.
      iApply (big_sepL_impl (λ k _, ∃ (v : val) (ty : type), ⌜vec_to_list xl !! k = Some v⌝ ∗ ⌜vec_to_list tys !! k = Some ty⌝ ∗ v ◁ ty)%I with "[Hxl]"). {
        move: xl. rewrite fmap_length gen_binders_length => xl. clear.
        iInduction n as [|n] "IH" => //=. inv_vec tys. inv_vec xl =>* /=.
        iDestruct "Hxl" as "[Hx Hxl]".
        iSplitL "Hx"; last by iApply "IH".
        by iExists _,_; iFrame; eauto.
      }
      iIntros "!#" (k s Hk) "Hv Hp".
      rewrite 3!subst_env_expr subst_env_closed lookup_insert /=.
      iDestruct "Hv" as (v ty Hv Hty) "Hty".
      rewrite subst_env_expr lookup_insert_ne. 2: {
        move: Hk. intros Hk%elem_of_list_lookup_2. set_unfold.
        move: Hk => [? [-> ?]]. move => /BNamed_inj Hx.
        eapply pretty_nat_neq. by eauto.
      }
      rewrite (subst_map_in _ _ k s v) //=.
      have := lookup_lt_Some _ _ _ Hty. rewrite vec_to_list_length =>?.
      iApply typed_expr_mono; last iApply (type_write with "Hp Hty"); eauto; try lia.
      + rewrite Nat2Z.id insert_app_r_alt take_length_le -?minus_n_n ?vec_to_list_length; [|lia..].
        have -> : ((n - k) = S (n - S k))%nat by lia. simpl.
        rewrite cons_middle (take_S_r _ _ _ Hty) assoc. by iIntros "?".
      + rewrite app_length take_length_le ?replicate_length ?vec_to_list_length; lia.
    - rewrite imap_length gen_binders_length vec_to_list_length -minus_n_n /= app_nil_r -{1}(vec_to_list_length tys) firstn_all.
      by iApply type_val.
  Qed.

  Global Opaque new_tuple.

End product.
