From Coq Require Import Min.
From stdpp Require Import coPset namespaces sets list.
From iris.algebra Require Import big_op gmap frac agree list.
From iris.algebra Require Import csum excl auth cmra_big_op.
From iris.bi Require Import fractional.
From iris.base_logic Require Export lib.own lib.invariants.
From iris.proofmode Require Export tactics.
From sandbox.lang Require Export lang syscall_policies.
Set Default Proof Using "Type".
Import uPred.

Definition heapUR : ucmraT :=
  gmapUR loc (prodR fracR (agreeR valO)).

Definition heap_freeableUR : ucmraT :=
  gmapUR block (prodR fracR (gmapR Z (exclR unitO))).

Definition traceUR : ucmraT := listUR (optionUR (agreeR obsO)).

(** prevent circularity if heap_SP refers to maps_to, obs... Safety
properties should use sandboxSPPreG instead of sandboxG *)
Class sandboxSPPreG Σ := SandboxSPPreG {
  heap_axioms :> syscallG;
  heap_inG :> inG Σ (authR heapUR);
  heap_freeable_inG :> inG Σ (authR heap_freeableUR);
  heap_trace_inG :> inG Σ (authR traceUR);
  heap_interface_inG :> inG Σ (agreeR interfaceO);
  heap_name : gname;
  heap_freeable_name : gname;
  heap_trace_name : gname;
  heap_interface_name : gname;
}.

Class heapG Σ := HeapG {
  heap_sandboxPre :> sandboxSPPreG Σ;
  heap_SP : syscall_policyG Σ;
  heap_spg_name : heap_SP.(spg_name);
  heap_spg_axioms_equiv : ∀ π, (sysaxioms π ↔ heap_SP.(spg_axioms) π);
}.

Definition to_heap : state → heapUR :=
  fmap (λ v, (1%Qp, to_agree v)) ∘ (filter (λ kv, high kv.1)) ∘ heap.
Definition heap_freeable_rel (σ : state) (hF : heap_freeableUR) : Prop :=
  ∀ hblk qs, hF !! hblk = Some qs →
    qs.2 ≠ ∅ ∧ ∀ i, is_Some (σ.(heap) !! (High, hblk, i)) ↔ is_Some (qs.2 !! i).

Definition to_trace : list observation → traceUR :=
  fmap Some ∘ fmap to_agree.

Section sp_definitions.
  Context `{!sandboxSPPreG Σ}.

  Definition heap_surface (σ : state) : Prop :=
    map_Forall (λ _ v, surface_val v) σ.(heap).

  (* l must be high because otherwise we could not get the token *)
  Definition heap_mapsto_st
             (l : loc) (q : Qp) (v: val) : iProp Σ :=
    (⌜surface_val v⌝ ∗ own heap_name (◯ {[ l := (q, to_agree v) ]}))%I.

  Definition heap_mapsto_def (l : loc) (q : Qp) (v: val) : iProp Σ :=
    heap_mapsto_st l q v.
  Definition heap_mapsto_aux : seal (@heap_mapsto_def). by eexists. Qed.
  Definition heap_mapsto := unseal heap_mapsto_aux.
  Definition heap_mapsto_eq : @heap_mapsto = @heap_mapsto_def :=
    seal_eq heap_mapsto_aux.

  Definition heap_mapsto_vec (l : loc) (q : Qp) (vl : list val) : iProp Σ :=
    ([∗ list] i ↦ v ∈ vl, heap_mapsto (l +ₗ i) q v)%I.

  Fixpoint inter (i0 : Z) (n : nat) : gmapR Z (exclR unitO) :=
    match n with O => ∅ | S n => <[i0 := Excl ()]>(inter (i0+1) n) end.

  Definition heap_freeable_def (l : loc) (q : Qp) (n: nat) : iProp Σ :=
    (⌜high l⌝ ∗ own heap_freeable_name (◯ {[ l.1.2 := (q, inter (l.2) n) ]}))%I.
  Definition heap_freeable_aux : seal (@heap_freeable_def). by eexists. Qed.
  Definition heap_freeable := unseal heap_freeable_aux.
  Definition heap_freeable_eq : @heap_freeable = @heap_freeable_def :=
    seal_eq heap_freeable_aux.

  (** observations in the trace *)
  Definition full_trace_def (π : list observation) : iProp Σ :=
      own heap_trace_name (● to_trace π).
  Definition full_trace_aux : seal (@full_trace_def). by eexists. Qed.
  Definition full_trace := unseal full_trace_aux.
  Definition full_trace_eq : @full_trace = @full_trace_def := seal_eq full_trace_aux.

  Definition in_trace_def (i : nat) (obs : observation) : iProp Σ :=
      own heap_trace_name (◯ {[ i := Some (to_agree obs) ]}).
  Definition in_trace_aux : seal (@in_trace_def). by eexists. Qed.
  Definition in_trace := unseal in_trace_aux.
  Definition in_trace_eq : @in_trace = @in_trace_def := seal_eq in_trace_aux.

  Definition high_interface_def (i : interface_map): iProp Σ :=
      own heap_interface_name (to_agree i).
  Definition high_interface_aux : seal (@high_interface_def). by eexists. Qed.
  Definition high_interface := unseal high_interface_aux.
  Definition high_interface_eq : @high_interface = @high_interface_def := seal_eq high_interface_aux.

  Definition interface_surface (σ : state) : Prop :=
    ∀ p, map_Forall (λ _ v, surface_val v) (σ.(interface) p).

  Global Instance high_interface_persistent I : Persistent (high_interface I).
  Proof. rewrite high_interface_eq. apply _. Qed.
End sp_definitions.

Section heap_definitions.
  Context `{!heapG Σ}.

  Definition sp_rel (sps : heap_SP.(spg_SP).(sp_state))(σ : state) : Prop :=
    run_sp_trace heap_SP.(spg_SP) σ.(trace) = Some sps.

  Definition heap_ctx (σ : state) : iProp Σ :=
    (∃ hF sps, own heap_name (● to_heap σ)
            ∗ own heap_freeable_name (● hF)
            ∗ high_interface (σ.(interface) High)
            ∗ full_trace σ.(trace)
            ∗ (heap_SP.(spg_prop) heap_spg_name sps)
            ∗ ⌜heap_freeable_rel σ hF⌝
            ∗ ⌜heap_surface σ⌝
            ∗ ⌜interface_surface σ⌝
            ∗ (⌜sp_rel sps σ⌝))%I.

  Global Instance full_trace_timeless π : Timeless (full_trace π).
  Proof. rewrite full_trace_eq. apply _. Qed.

End heap_definitions.

Typeclasses Opaque heap_mapsto heap_freeable heap_mapsto_vec.
Instance: Params (@heap_mapsto) 4.
Instance: Params (@heap_freeable) 5.

Notation "l ↦{ q } v" := (heap_mapsto l q v)
  (at level 20, q at level 50, format "l  ↦{ q }  v") : bi_scope.
Notation "l ↦ v" := (heap_mapsto l 1 v) (at level 20) : bi_scope.

Notation "l ↦∗{ q } vl" := (heap_mapsto_vec l q vl)
  (at level 20, q at level 50, format "l  ↦∗{ q }  vl") : bi_scope.
Notation "l ↦∗ vl" := (heap_mapsto_vec l 1 vl) (at level 20) : bi_scope.

Notation "l ↦∗{ q }: P" := (∃ vl, l ↦∗{ q } vl ∗ P vl)%I
  (at level 20, q at level 50, format "l  ↦∗{ q }:  P") : bi_scope.
Notation "l ↦∗: P " := (∃ vl, l ↦∗ vl ∗ P vl)%I
  (at level 20, format "l  ↦∗:  P") : bi_scope.

Notation "†{ q } l … n" := (heap_freeable l q n)
  (at level 20, q at level 50, format "†{ q } l … n") : bi_scope.
Notation "† l … n" := (heap_freeable l 1 n) (at level 20) : bi_scope.
Notation inSP Σ subSP := (inSP' Σ subSP heap_SP).

Section to_heap.
  Implicit Types σ : state.

  Lemma to_heap_valid σ : ✓ to_heap σ.
  Proof. intros l. rewrite lookup_fmap. set m := filter _ _. case (m !! l) => [v|] //=. Qed.

  Lemma lookup_to_heap_None σ l : σ.(heap) !! l = None → to_heap σ !! l = None.
  Proof. by rewrite /to_heap lookup_fmap fmap_None map_filter_lookup_None; left. Qed.

  Lemma to_heap_insert_high σ l v :
    high l →
    to_heap $ heap_fupd (insert l v) σ
    = <[l:=(1%Qp, to_agree v)]> (to_heap σ).
  Proof. move => ?. by rewrite /to_heap /= map_filter_insert // fmap_insert. Qed.

  Lemma to_heap_insert_low σ l v :
    low l →
    to_heap $ heap_fupd (insert l v) σ
    = (to_heap σ).
  Proof.
    move => ?.
    rewrite /to_heap /= map_filter_insert_not //= => ? ?.
    by apply: low_high_excl.
  Qed.

  Lemma to_heap_delete σ l : to_heap (heap_fupd (delete l) σ) = delete l (to_heap σ).
  Proof. by rewrite /to_heap /= map_filter_delete fmap_delete. Qed.

  Lemma to_heap_delete_low σ l :
    low l →
    to_heap (heap_fupd (delete l) σ) = (to_heap σ).
  Proof.
    move => ?.
    rewrite /to_heap /= map_filter_delete_not //= => ? ?.
      by apply: low_high_excl.
  Qed.
End to_heap.

Section heap_surface.
  Context `{!heapG Σ}.
  Implicit Types σ : state.

  Lemma heap_surface_init_mem l n σ:
    heap_surface σ → heap_surface (init_mem l n σ).
  Proof.
    rewrite /heap_surface. move => Hrel. elim: n l => //= n IH l.
    by apply: map_Forall_insert_2.
  Qed.

  Lemma heap_surface_free_mem l n σ:
    heap_surface σ → heap_surface (free_mem l n σ).
  Proof.
    rewrite /heap_surface. move => Hrel. elim: n l => //= n IH l.
    by apply: map_Forall_delete.
  Qed.
End heap_surface.

Section high_interface.
  Context `{heapG Σ}.
  Implicit Types σ : state.

  Lemma interface_init_mem l n σ:
    interface (init_mem l n σ) = interface σ.
  Proof. elim: n l => //=. Qed.

  Lemma interface_free_mem l n σ:
    interface (free_mem l n σ) = interface σ.
  Proof. elim: n l => //=. Qed.

  Lemma high_interface_state σ I:
    heap_ctx σ -∗ high_interface I -∗ ⌜I = σ.(interface) High⌝.
  Proof.
    iDestruct 1 as (? ?) "(_ & _ & Hσ & _)". iIntros "HI".
    rewrite high_interface_eq.
    iDestruct (own_valid_2 with "HI Hσ") as %Hv. iPureIntro.
    by apply agree_op_invL'.
  Qed.

  Lemma interface_surface_init_mem l n σ:
    interface_surface (init_mem l n σ) = interface_surface σ.
  Proof. by rewrite /interface_surface interface_init_mem. Qed.

  Lemma interface_surface_free_mem l n σ:
    interface_surface (free_mem l n σ) = interface_surface σ.
  Proof. by rewrite /interface_surface interface_free_mem. Qed.

  Lemma interface_lookup_surface σ p s v:
    heap_ctx σ -∗ ⌜σ.(interface) p !! s = Some v⌝ -∗ ⌜surface_val v⌝.
  Proof.
    iDestruct 1 as (hF sps) "(_ & _ & _ & _ & _ & _ & _ &Hs& _)".
    iRevert "Hs". iPureIntro => Hs ?. by eapply Hs.
  Qed.

End high_interface.

Section trace.
  Context `{heapG Σ}.
  Implicit Types σ : state.

  Lemma trace_init_mem l n σ:
    trace (init_mem l n σ) = trace σ.
  Proof. elim: n l => //=. Qed.

  Lemma trace_free_mem l n σ:
    trace (free_mem l n σ) = trace σ.
  Proof. elim: n l => //=. Qed.

  Lemma to_trace_length π : length (to_trace π) = length π.
  Proof. by rewrite /to_trace /= !fmap_length. Qed.

  Lemma to_trace_app π obs :
    (to_trace π) ++ [Some (to_agree obs)] = to_trace (π ++ [obs]).
  Proof. by rewrite /to_trace /= !fmap_app. Qed.

  Lemma to_trace_lookup π i o:
    to_trace π !! i ≡ Some (Some (to_agree o)) ↔
    π !! i = Some o.
  Proof.
    rewrite /to_trace /= !list_lookup_fmap /=. split; last by move => ->.
    have [[? ->]|/eq_None_not_Some ->] := decide (is_Some (π !! i))=>/=.
    - by intros <-%Some_equiv_inj%Some_equiv_inj%to_agree_inj%leibniz_equiv.
    - move => Heq. symmetry in Heq. by move/equiv_None in Heq.
  Qed.

  Global Instance in_trace_persistent i o : Persistent (in_trace i o).
  Proof. rewrite in_trace_eq /in_trace_def. apply _. Qed.

  Lemma in_full_trace π i o:
    full_trace π -∗ in_trace i o -∗ ⌜π !! i = Some o⌝.
  Proof.
    rewrite full_trace_eq in_trace_eq. iIntros "Hπ Hi".
    iDestruct (own_valid_2 with "Hπ Hi")as%[Hl Hv]%auth_both_valid.
    iPureIntro. move: Hl => /list_lookup_included Hl. move: {Hl} (Hl i).
    rewrite list_lookup_singletonM -to_trace_lookup => Hl.
    have [|x Hx] := is_Some_included _ _ Hl; first by econstructor.
    move: Hv => /list_lookup_valid Hv. move: {Hv} (Hv i) Hl.
    rewrite {}Hx => /Some_valid Hx /Some_included [<-|] // Hl.
    have [|y Hy] := is_Some_included _ _ Hl;first by econstructor. subst.
    by move: Hx Hl => /Some_valid/to_agree_uninj[? <-] /Some_included [<-|] // /to_agree_included ->.
  Qed.

  Lemma in_full_trace_idx π i o:
    full_trace π -∗ in_trace i o -∗ ⌜length π > i⌝%nat.
  Proof.
    iIntros "Hπ Hin".
    iDestruct (in_full_trace with "Hπ Hin") as %?. iPureIntro.
      by eapply lookup_lt_Some.
  Qed.

  Lemma in_full_trace_idxs π idxs:
    full_trace π -∗ ([∗ list] i ∈ idxs, ∃ o, in_trace i o) -∗
      ⌜(length π) > max_list_Z idxs⌝.
  Proof.
    iInduction idxs as [|] "IH"; first by iIntros "_ _ !% /="; lia.
    iIntros "Hπ [Hin Hlist]". iDestruct "Hin" as (?) "Hin".
    iDestruct (in_full_trace_idx with "Hπ Hin") as %? => /=.
    iDestruct ("IH" with "Hπ [$]") as %?. iPureIntro. lia.
  Qed.

  Lemma full_trace_add_obs s v r π:
    full_trace π ==∗ full_trace (π ++ [(s, v, r)]) ∗ in_trace (length π) (s, v, r).
  Proof.
    rewrite full_trace_eq in_trace_eq -own_op -to_trace_app -to_trace_length.
    iApply own_update.
    by eapply auth_update_alloc, list_alloc_singleton_local_update.
  Qed.

End trace.

Section sp_rel.
  Context `{heapG Σ}.
  Implicit Types σ : state.

  Lemma sp_rel_heap_fupd f sps σ:
    sp_rel sps (heap_fupd f σ) = sp_rel sps σ.
  Proof. done. Qed.

  Lemma sp_rel_init_mem l n sps σ:
    sp_rel sps (init_mem l n σ) = sp_rel sps σ.
  Proof. elim: n l => //= ? IH ?. by rewrite sp_rel_heap_fupd. Qed.

  Lemma sp_rel_free_mem l n sps σ:
    sp_rel sps (free_mem l n σ) = sp_rel sps σ.
  Proof. elim: n l => //= ? IH ?. by rewrite sp_rel_heap_fupd. Qed.

  Lemma sp_rel_add_obs `{inSP Σ subSP} sps sps' σ s n t:
    run_sp_obs subSP (t, s, n) (insp_state subSP sps) = Some sps' →
    sp_rel sps σ →
    sp_rel (insp_state_upd subSP sps' sps) (add_observation (t, s, n) σ).
  Proof.
    rewrite /sp_rel. move => Hsps Hr /=.
    rewrite run_sp_trace_app Hr /=.
    by apply insp_trans.
  Qed.

End sp_rel.

Section heap.
  Context `{heapG Σ}.
  Implicit Types P Q : iProp Σ.
  Implicit Types σ : state.
  Implicit Types E : coPset.

  (** General properties of mapsto and freeable *)
  Global Instance heap_mapsto_timeless l q v : Timeless (l↦{q}v).
  Proof. rewrite heap_mapsto_eq /heap_mapsto_def. apply _. Qed.

  Global Instance heap_mapsto_fractional l v: Fractional (λ q, l ↦{q} v)%I.
  Proof.
    intros p q.
    rewrite heap_mapsto_eq assoc (comm bi_sep _ (⌜_⌝)%I) assoc -(assoc bi_sep _ _ (own _ _)).
    by rewrite -own_op -auth_frag_op op_singleton -pair_op agree_idemp -persistent_sep_dup.
  Qed.
  Global Instance heap_mapsto_as_fractional l q v:
    AsFractional (l ↦{q} v) (λ q, l ↦{q} v)%I q.
  Proof. split. done. apply _. Qed.

  Global Instance heap_mapsto_vec_timeless l q vl : Timeless (l ↦∗{q} vl).
  Proof. rewrite /heap_mapsto_vec. apply _. Qed.

  Global Instance heap_mapsto_vec_fractional l vl: Fractional (λ q, l ↦∗{q} vl)%I.
  Proof.
    intros p q. rewrite /heap_mapsto_vec -big_sepL_sep.
    by setoid_rewrite (fractional (Φ := λ q, _ ↦{q} _)%I).
  Qed.
  Global Instance heap_mapsto_vec_as_fractional l q vl:
    AsFractional (l ↦∗{q} vl) (λ q, l ↦∗{q} vl)%I q.
  Proof. split. done. apply _. Qed.

  Global Instance heap_freeable_timeless q l n : Timeless (†{q}l…n).
  Proof. rewrite heap_freeable_eq /heap_freeable_def. apply _. Qed.

  Lemma heap_mapsto_agree l q1 q2 v1 v2 : l ↦{q1} v1 ∗ l ↦{q2} v2 ⊢ ⌜v1 = v2⌝.
  Proof.
    rewrite heap_mapsto_eq /heap_mapsto_def /heap_mapsto_st 2!(sep_elim_r ⌜_⌝%I).
    rewrite -own_op -auth_frag_op own_valid discrete_valid.
    eapply pure_elim; [done|]=> /auth_frag_valid /=.
    rewrite op_singleton -pair_op singleton_valid=> -[? /agree_op_invL'->]; eauto.
  Qed.

  Lemma heap_mapsto_vec_nil l q : l ↦∗{q} [] ⊣⊢ True.
  Proof. by rewrite /heap_mapsto_vec. Qed.

  Lemma heap_mapsto_vec_app l q vl1 vl2 :
    l ↦∗{q} (vl1 ++ vl2) ⊣⊢ l ↦∗{q} vl1 ∗ (l +ₗ length vl1) ↦∗{q} vl2.
  Proof.
    rewrite /heap_mapsto_vec big_sepL_app.
    do 2 f_equiv. intros k v. by rewrite shift_loc_assoc_nat.
  Qed.

  Lemma heap_mapsto_vec_singleton l q v : l ↦∗{q} [v] ⊣⊢ l ↦{q} v.
  Proof. by rewrite /heap_mapsto_vec /= shift_loc_0 right_id. Qed.

  Lemma heap_mapsto_vec_cons l q v vl:
    l ↦∗{q} (v :: vl) ⊣⊢ l ↦{q} v ∗ (l +ₗ 1) ↦∗{q} vl.
  Proof.
    by rewrite (heap_mapsto_vec_app l q [v] vl) heap_mapsto_vec_singleton.
  Qed.

  Lemma heap_mapsto_vec_op l q1 q2 vl1 vl2 :
    length vl1 = length vl2 →
    l ↦∗{q1} vl1 ∗ l ↦∗{q2} vl2 ⊣⊢ ⌜vl1 = vl2⌝ ∧ l ↦∗{q1+q2} vl1.
  Proof.
    intros Hlen%Forall2_same_length. apply (anti_symm (⊢)).
    - revert l. induction Hlen as [|v1 v2 vl1 vl2 _ _ IH]=> l.
      { rewrite !heap_mapsto_vec_nil. iIntros "_"; auto. }
      rewrite !heap_mapsto_vec_cons. iIntros "[[Hv1 Hvl1] [Hv2 Hvl2]]".
      iDestruct (IH (l +ₗ 1) with "[$Hvl1 $Hvl2]") as "[% $]"; subst.
      rewrite (inj_iff (.:: vl2)).
      iDestruct (heap_mapsto_agree with "[$Hv1 $Hv2]") as %<-.
      iSplit; first done. iFrame.
    - by iIntros "[% [$ Hl2]]"; subst.
  Qed.

  Lemma heap_mapsto_pred_op l q1 q2 n (Φ : list val → iProp Σ) :
    (∀ vl, Φ vl -∗ ⌜length vl = n⌝) →
    l ↦∗{q1}: Φ ∗ l ↦∗{q2}: (λ vl, ⌜length vl = n⌝) ⊣⊢ l ↦∗{q1+q2}: Φ.
  Proof.
    intros Hlen. iSplit.
    - iIntros "[Hmt1 Hmt2]".
      iDestruct "Hmt1" as (vl) "[Hmt1 Hown]". iDestruct "Hmt2" as (vl') "[Hmt2 %]".
      iDestruct (Hlen with "Hown") as %?.
      iCombine "Hmt1" "Hmt2" as "Hmt". rewrite heap_mapsto_vec_op; last congruence.
      iDestruct "Hmt" as "[_ Hmt]". iExists vl. by iFrame.
    - iIntros "Hmt". iDestruct "Hmt" as (vl) "[[Hmt1 Hmt2] Hown]".
      iDestruct (Hlen with "Hown") as %?.
      iSplitL "Hmt1 Hown"; iExists _; by iFrame.
  Qed.

  Lemma heap_mapsto_pred_wand l q Φ1 Φ2 :
    l ↦∗{q}: Φ1 -∗ (∀ vl, Φ1 vl -∗ Φ2 vl) -∗ l ↦∗{q}: Φ2.
  Proof.
    iIntros "Hm Hwand". iDestruct "Hm" as (vl) "[Hm HΦ]". iExists vl.
    iFrame "Hm". by iApply "Hwand".
  Qed.

  Lemma heap_mapsto_pred_iff_proper l q Φ1 Φ2 :
    □ (∀ vl, Φ1 vl ↔ Φ2 vl) -∗ □ (l ↦∗{q}: Φ1 ↔ l ↦∗{q}: Φ2).
  Proof.
    iIntros "#HΦ !#". iSplit; iIntros; iApply (heap_mapsto_pred_wand with "[-]"); try done; [|];
    iIntros; by iApply "HΦ".
  Qed.

  Lemma heap_mapsto_vec_combine l q vl :
    high l →
    vl ≠ [] →
    l ↦∗{q} vl ⊣⊢ ([∗ list] x ∈ vl, ⌜surface_val x⌝) ∗ own heap_name (◯ [^op list] i ↦ v ∈ vl,
      {[l +ₗ i := (q, to_agree v)]}).
  Proof.
    rewrite /heap_mapsto_vec heap_mapsto_eq /heap_mapsto_def /heap_mapsto_st=>??.
    by rewrite (big_opL_commute auth_frag) big_opL_commute1 // big_sepL_sep.
  Qed.

  Global Instance heap_mapsto_pred_fractional l (P : list val → iProp Σ):
    (∀ vl, Persistent (P vl)) → Fractional (λ q, l ↦∗{q}: P)%I.
  Proof.
    intros p q q'. iSplit.
    - iIntros "H". iDestruct "H" as (vl) "[[Hown1 Hown2] #HP]".
      iSplitL "Hown1"; eauto.
    - iIntros "[H1 H2]". iDestruct "H1" as (vl1) "[Hown1 #HP1]".
      iDestruct "H2" as (vl2) "[Hown2 #HP2]".
      set (ll := min (length vl1) (length vl2)).
      rewrite -[vl1](firstn_skipn ll) -[vl2](firstn_skipn ll) 2!heap_mapsto_vec_app.
      iDestruct "Hown1" as "[Hown1 _]". iDestruct "Hown2" as "[Hown2 _]".
      iCombine "Hown1" "Hown2" as "Hown". rewrite heap_mapsto_vec_op; last first.
      { rewrite !firstn_length. subst ll.
        rewrite -!min_assoc min_idempotent min_comm -min_assoc min_idempotent min_comm. done. }
      iDestruct "Hown" as "[H Hown]". iDestruct "H" as %Hl. iExists (take ll vl1). iFrame.
      destruct (min_spec (length vl1) (length vl2)) as [[Hne Heq]|[Hne Heq]].
      + iClear "HP2". rewrite take_ge; last first.
        { rewrite -Heq /ll. done. }
        rewrite drop_ge; first by rewrite app_nil_r. by rewrite -Heq.
      + iClear "HP1". rewrite Hl take_ge; last first.
        { rewrite -Heq /ll. done. }
        rewrite drop_ge; first by rewrite app_nil_r. by rewrite -Heq.
  Qed.
  Global Instance heap_mapsto_pred_as_fractional l q (P : list val → iProp Σ):
    (∀ vl, Persistent (P vl)) → AsFractional (l ↦∗{q}: P) (λ q, l ↦∗{q}: P)%I q.
  Proof. split. done. apply _. Qed.

  Lemma inter_lookup_Some i j (n : nat):
    i ≤ j < i+n → inter i n !! j = Excl' ().
  Proof.
    revert i. induction n as [|n IH]=>/= i; first lia.
    rewrite lookup_insert_Some. destruct (decide (i = j)); naive_solver lia.
  Qed.
  Lemma inter_lookup_None i j (n : nat):
    j < i ∨ i+n ≤ j → inter i n !! j = None.
  Proof.
    revert i. induction n as [|n IH]=>/= i; first by rewrite lookup_empty.
    rewrite lookup_insert_None. naive_solver lia.
  Qed.
  Lemma inter_op i n n' : inter i n ⋅ inter (i+n) n' ≡ inter i (n+n').
  Proof.
    intros j. rewrite lookup_op.
    destruct (decide (i ≤ j < i+n)); last destruct (decide (i+n ≤ j < i+n+n')).
    - by rewrite (inter_lookup_None (i+n) j n') ?inter_lookup_Some; try lia.
    - by rewrite (inter_lookup_None i j n) ?inter_lookup_Some; try lia.
    - by rewrite !inter_lookup_None; try lia.
  Qed.
  Lemma inter_valid i n : ✓ inter i n.
  Proof. revert i. induction n as [|n IH]=>i. done. by apply insert_valid. Qed.

  (* Lemma heap_freeable_op_eq l q1 q2 n n' : *)
    (* †{q1}l…n ∗ †{q2}l+ₗn … n' ⊣⊢ †{q1+q2}l…(n+n'). *)
  (* Proof. *)
    (* by rewrite heap_freeable_eq /heap_freeable_def -own_op -auth_frag_op *)
      (* op_singleton pair_op inter_op. *)
    (* Qed. *)

  (** Properties about heap_freeable_rel and heap_freeable *)
  Lemma heap_freeable_rel_None σ l hF :
    high l →
    (∀ m : Z, σ.(heap) !! (l +ₗ m) = None) →
    heap_freeable_rel σ hF →
    hF !! l.1.2 = None.
  Proof.
    intros HIGH FRESH REL. apply eq_None_not_Some. intros [[q s] [Hsne REL']%REL].
    destruct (map_choose s) as [i []%REL'%not_eq_None_Some]; first done.
    move: (FRESH (i - l.2)). by rewrite /shift_loc Zplus_minus -HIGH.
  Qed.

  Lemma heap_freeable_is_Some σ hF l n i :
    high l →
    heap_freeable_rel σ hF →
    hF !! l.1.2 = Some (1%Qp, inter (l.2) n) →
    is_Some (σ.(heap) !! (l +ₗ i)) ↔ 0 ≤ i ∧ i < n.
  Proof.
    destruct l as [b j]; rewrite /shift_loc /=. intros HIGH REL Hl.
    case (REL b.2 (1%Qp, inter j n)) => // _.
    rewrite -HIGH /= -surjective_pairing => ->.
    destruct (decide (0 ≤ i ∧ i < n)).
    - rewrite is_Some_alt inter_lookup_Some; lia.
    - rewrite is_Some_alt inter_lookup_None; lia.
  Qed.

  Lemma heap_freeable_rel_init_mem_high l h n σ:
    high l →
    n ≠ O →
    (∀ m : Z, σ.(heap) !! (l +ₗ m) = None) →
    heap_freeable_rel σ h →
    heap_freeable_rel (init_mem l n σ)
                      (<[l.1.2 := (1%Qp, inter (l.2) n)]> h).
  Proof.
    move=> HIGH Hvlnil FRESH REL blk qs /lookup_insert_Some [[<- <-]|[??]].
    - rewrite -HIGH -surjective_pairing.
      split.
      { destruct n as [|n]; simpl in *; [done|]. apply: insert_non_empty. }
      intros i. destruct (decide (l.2 ≤ i ∧ i < l.2 + n)).
      + rewrite inter_lookup_Some // lookup_init_mem //. naive_solver.
      + rewrite inter_lookup_None; last lia. rewrite lookup_init_mem_ne /=; last lia.
        replace (l.1,i) with (l +ₗ (i - l.2)) by (rewrite /shift_loc; f_equal; lia).
        by rewrite FRESH !is_Some_alt.
    - destruct (REL blk qs) as [? Hs]; auto.
      split; [done|]=> i. rewrite -Hs lookup_init_mem_ne //= (surjective_pairing l.1).
        by left => [[]].
  Qed.

  Lemma heap_freeable_rel_init_mem_low l h n σ:
    low l →
    heap_freeable_rel σ h →
    heap_freeable_rel (init_mem l n σ) h.
  Proof.
    move=> LOW REL blk qs Hblk.
    have {REL} [? Hrel] := REL blk qs Hblk.
    setoid_rewrite <-Hrel. split => // i.
    elim: n l LOW => //= n IH l LOW.
    rewrite lookup_insert_ne; first by eauto using low_shift.
    by rewrite (surjective_pairing l) (surjective_pairing l.1) LOW.
  Qed.

  Lemma heap_freeable_rel_free_mem_high σ hF n l :
    hF !! l.1.2 = Some (1%Qp, inter (l.2) n) →
    heap_freeable_rel σ hF →
    heap_freeable_rel (free_mem l n σ) (delete (l.1.2) hF).
  Proof.
    intros Hl REL b qs; rewrite lookup_delete_Some=> -[NEQ ?].
    destruct (REL b qs) as [? REL']; auto.
    split; [done|]=> i. by rewrite -REL' lookup_free_mem_ne //= (surjective_pairing l.1) => [[]].
  Qed.

  Lemma heap_freeable_rel_free_mem_low σ hF n l :
    low l →
    heap_freeable_rel σ hF →
    heap_freeable_rel (free_mem l n σ) hF.
  Proof.
    intros Hl REL b qs Hblk.
    destruct (REL b qs) as [? REL']; auto.
    split; [done|]=> i. rewrite -REL'.
    elim: n l Hl => //= n IH l Hl.
    rewrite lookup_delete_ne; first by eauto using low_shift.
    by rewrite (surjective_pairing l) (surjective_pairing l.1) Hl.
  Qed.

  Lemma heap_freeable_rel_stable σ h l p :
    heap_freeable_rel σ h → is_Some (σ.(heap) !! l) →
    heap_freeable_rel (heap_fupd (<[l := p]>) σ) h.
  Proof.
    intros REL Hσ blk qs Hqs. destruct (REL blk qs) as [? REL']; first done.
    split; [done|]=> i. rewrite -REL' lookup_insert_is_Some.
    destruct (decide (l = (High, blk, i))); naive_solver.
  Qed.

  Lemma heap_freeable_rel_stable_low σ h l p :
    low l →
    heap_freeable_rel σ h →
    heap_freeable_rel (heap_fupd (<[l := p]>) σ) h.
  Proof.
    intros Hl REL blk qs Hqs. destruct (REL blk qs) as [? REL']; first done.
    split; [done|]=> i. rewrite -REL' lookup_insert_is_Some.
    destruct (decide (l = (High, blk, i))); naive_solver.
  Qed.

  (** Weakest precondition for high *)
  Lemma heap_alloc_vs_high σ l n :
    high l →
    (∀ m : Z, σ.(heap) !! (l +ₗ m) = None) →
    own heap_name (● to_heap σ)
    ==∗ own heap_name (● to_heap (init_mem l n σ))
       ∗ own heap_name (◯ [^op list] i ↦ v ∈ (replicate n (LitV LitPoison)),
           {[l +ₗ i := (1%Qp, to_agree v)]}).
  Proof.
    intros HIGH FREE. rewrite -own_op. apply own_update, auth_update_alloc.
    revert l HIGH FREE. induction n as [|n IH]=> l HIGH FRESH; [done|].
    rewrite (big_opL_consZ_l (λ k _, _ (_ k) _ )) /=.
    etrans; first apply (IH (l +ₗ 1)). by apply high_shift.
    { intros. by rewrite shift_loc_assoc. }
    rewrite shift_loc_0 -insert_singleton_op; last first.
    { rewrite -equiv_None big_opL_commute equiv_None big_opL_None=> l' v' ?.
      rewrite lookup_singleton_None -{2}(shift_loc_0 l). apply not_inj; lia. }
    rewrite to_heap_insert_high //. setoid_rewrite shift_loc_assoc.
    apply alloc_local_update; last done. apply lookup_to_heap_None.
    rewrite lookup_init_mem_ne /=; last lia. by rewrite -(shift_loc_0 l).
  Qed.

  Lemma heap_alloc_high σ l n :
    high l →
    0 < n →
    (∀ m, σ.(heap) !! (l +ₗ m) = None) →
    heap_ctx σ ==∗
      heap_ctx (init_mem l (Z.to_nat n) σ) ∗ †l…(Z.to_nat n) ∗
      l ↦∗ replicate (Z.to_nat n) (LitV LitPoison).
  Proof.
    intros ???; iDestruct 1 as (hF sps) "(Hvalσ & HhF & HI & HhT & Hsps & % & % & #?)".
    assert (Z.to_nat n ≠ O). { rewrite -(Nat2Z.id 0)=>/Z2Nat.inj. lia. }
    iMod (heap_alloc_vs_high _ _ (Z.to_nat n) with "[$Hvalσ]") as "[Hvalσ Hmapsto]" => //.
    iMod (own_update _ (● hF) with "HhF") as "[HhF Hfreeable]".
    { apply auth_update_alloc,
        (alloc_singleton_local_update _ (l.1.2) (1%Qp, inter (l.2) (Z.to_nat n))).
      - eauto using heap_freeable_rel_None.
      - split. done. apply inter_valid. }
    iModIntro. iSplitL "Hvalσ HhF HhT Hsps HI".
    { iExists _, _. rewrite sp_rel_init_mem trace_init_mem interface_init_mem interface_surface_init_mem. iFrame.
      iSplitR; eauto using heap_surface_init_mem, heap_freeable_rel_init_mem_high. }
    rewrite heap_freeable_eq /heap_freeable_def heap_mapsto_vec_combine //; last by destruct (Z.to_nat n).
    iFrame => //. iSplit => //. rewrite big_sepL_forall. by iIntros (??[-> _]%lookup_replicate).
  Qed.

  Lemma heap_free_vs_high σ l vl :
    own heap_name (● to_heap σ)
  ∗ own heap_name (◯ [^op list] i ↦ v ∈ vl,
                     {[l +ₗ i := (1%Qp, to_agree v)]})
    ==∗ own heap_name (● to_heap (free_mem l (length vl) σ)).
  Proof.
    rewrite -own_op. apply own_update, auth_update_dealloc.
    revert σ l. induction vl as [|v vl IH]=> σ l; [done|].
    rewrite (big_opL_consZ_l (λ k _, _ (_ k) _ )) /= shift_loc_0.
    apply local_update_total_valid=> _ Hvalid _.
    assert (([^op list] k↦y ∈ vl,
      {[l +ₗ (1 + k) := (1%Qp, to_agree y)]} : heapUR) !! l = None).
    { move: (Hvalid l). rewrite lookup_op lookup_singleton.
      by move=> /(cmra_discrete_valid_iff 0) /exclusiveN_Some_l. }
    rewrite -insert_singleton_op //. etrans.
    { apply (delete_local_update _ _ l (1%Qp, to_agree v)).
      by rewrite lookup_insert. }
    rewrite delete_insert // -to_heap_delete delete_free_mem.
    setoid_rewrite <-shift_loc_assoc. apply IH.
  Qed.

  Lemma heap_free_high σ l vl (n : Z) E:
    n = length vl →
    heap_ctx σ -∗ l ↦∗ vl -∗ †l…(length vl)
    ==∗ ⌜0 < n⌝ ∗ ⌜∀ m, is_Some (σ.(heap) !! (l +ₗ m)) ↔ (0 ≤ m < n)⌝ ∗
        heap_ctx (free_mem l (Z.to_nat n) σ).
  Proof.
    iDestruct 1 as (hF sps) "(Hvalσ & HhF & ? & ? & Hsps & REL & % & % & #?)"; iDestruct "REL" as %REL.
    rewrite heap_freeable_eq /heap_freeable_def.
    iIntros "Hmt [% Hf]".
    iDestruct (own_valid_2 with "HhF Hf") as % [Hl Hv]%auth_both_valid.
    move: Hl=> /singleton_included [[q qs] [/leibniz_equiv_iff Hl Hq]].
    apply (Some_included_exclusive _ _) in Hq as [=<-<-]%leibniz_equiv; last first.
    { move: (Hv (l.1.2)). rewrite Hl. by intros [??]. }
    assert (vl ≠ []).
    { intros ->. by destruct (REL (l.1.2) (1%Qp, ∅)) as [[] ?]. }
    assert (0 < n) by (subst n; by destruct vl).
    iMod (heap_free_vs_high with "[Hmt Hvalσ]") as "Hvalσ".
    { rewrite heap_mapsto_vec_combine //. iDestruct "Hmt" as "[_ Hmt]". iFrame. }
    iMod (own_update_2 with "HhF Hf") as "HhF".
    { apply auth_update_dealloc, (delete_singleton_local_update _ _ _). }
    iModIntro; subst. repeat iSplit;  eauto using heap_freeable_is_Some.
    iExists _, _. subst. rewrite Nat2Z.id trace_free_mem interface_free_mem interface_surface_free_mem. iFrame.
    rewrite sp_rel_free_mem.
    by do ! iSplit; eauto using heap_freeable_rel_free_mem_high, heap_surface_free_mem.
  Qed.

  Lemma heap_mapsto_lookup σ l q v :
    own heap_name (● to_heap σ) -∗
    own heap_name (◯ {[ l := (q, to_agree v) ]}) -∗
    ⌜σ.(heap) !! l = Some (v)⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯") as %[Hl?]%auth_both_valid.
    iPureIntro. move: Hl=> /singleton_included [[q' dv]].
    rewrite /to_heap lookup_fmap fmap_Some_equiv.
    setoid_rewrite map_filter_lookup_Some.
    move => [[v' [Hf [/= -> ->]]]].
    move=> /Some_pair_included_total_2 [Hincl /to_agree_included->].
    by case Hf.
  Qed.

  Lemma heap_mapsto_vs_high σ l q v :
    own heap_name (● to_heap σ) -∗
    own heap_name (◯ {[ l := (q, to_agree v) ]}) -∗
    ⌜high l⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯") as %[Hl?]%auth_both_valid.
    iPureIntro. move: Hl=> /singleton_included [[q' dv]].
    rewrite /to_heap lookup_fmap fmap_Some_equiv.
    move => [[? [HF _]] _]. move: HF.
      by rewrite map_filter_lookup_Some => [[]].
  Qed.

  Lemma heap_mapsto_high l q v σ:
    heap_ctx σ -∗ l ↦{q} v -∗ ⌜high l⌝.
  Proof.
    iDestruct 1 as (hF sps) "(Hσ & HhF & ? & REL)".
    rewrite heap_mapsto_eq. iIntros "[_ Hmt]".
    iApply (heap_mapsto_vs_high with "Hσ"); eauto.
  Qed.

  Lemma heap_mapsto_surface l q v:
    l ↦{q} v -∗ ⌜surface_val v⌝.
  Proof. rewrite heap_mapsto_eq. by iDestruct 1 as (?) "_". Qed.

  Lemma heap_mapsto_vec_surface l q vs:
    l ↦∗{q} vs -∗ ⌜Forall surface_val vs⌝.
  Proof.
    rewrite /heap_mapsto_vec.
    iIntros "Hl".
    iAssert ([∗ list] i↦v ∈ vs, ⌜surface_val v⌝)%I with "[Hl]" as "Hvs". {
      iApply (big_sepL_impl with "Hl"). iIntros "!#" (???). iApply heap_mapsto_surface.
    }
    rewrite big_sepL_forall Forall_lookup. by iDestruct "Hvs" as %?.
  Qed.

  Lemma heap_freeable_high l q n:
    †{q}l…(n) -∗ ⌜high l⌝.
  Proof. rewrite heap_freeable_eq. by iDestruct 1 as "[? _]". Qed.

  Lemma heap_read_vs_high σ l q v:
    σ.(heap) !! l = Some v →
    own heap_name (● to_heap σ) -∗ heap_mapsto_st l q v
        ==∗ own heap_name (● to_heap (heap_fupd (<[l:=v]>) σ))
        ∗ heap_mapsto_st l q v.
  Proof.
    iIntros (Hσv) "Hown [$ Hm]".
    iDestruct (heap_mapsto_vs_high with "Hown Hm") as %HHigh.
    iRevert "Hown Hm". iApply wand_intro_r.
    rewrite /heap_mapsto_st -!own_op to_heap_insert_high //.
    eapply own_update, auth_update, singleton_local_update.
    { rewrite /to_heap lookup_fmap.
      apply fmap_Some_2.
      rewrite map_filter_lookup_Some Hσv.
        by split. }
    by apply prod_local_update_1.
  Qed.

  Lemma heap_read_high σ l q v:
    heap_ctx σ -∗ l ↦{q} v -∗ ⌜σ.(heap) !! l = Some v⌝.
  Proof.
    iDestruct 1 as (hF sps) "(Hσ & HhF & ?)".
    rewrite heap_mapsto_eq. iIntros "[_ Hmt]".
    by iDestruct (heap_mapsto_lookup with "Hσ Hmt") as %Hσl; eauto.
  Qed.

  Lemma heap_read_surface σ l v:
    heap_ctx σ -∗ ⌜σ.(heap) !! l = Some v⌝ -∗ ⌜surface_val v⌝.
  Proof.
    iDestruct 1 as (hF sps) "(_ & _ & _ & _ & _ & _ &%& _)". by eauto.
  Qed.

  Lemma heap_write_vs_high σ l v v':
    σ.(heap) !! l = Some v → surface_val v' →
    own heap_name (● to_heap σ) -∗ own heap_name (◯ {[l := (1%Qp, to_agree v)]})
    ==∗ own heap_name (● to_heap (heap_fupd (<[l:=(v')]>) σ))
        ∗ heap_mapsto_st l 1%Qp v'.
  Proof.
    iIntros (Hσv Hs) "Hown Hm".
    iDestruct (heap_mapsto_vs_high with "Hown Hm") as %HHigh.
    iAssert ⌜surface_val v'⌝%I as "$" => //.
    iRevert "Hown Hm". iApply wand_intro_r.
    rewrite -!own_op to_heap_insert_high //.
    eapply own_update, auth_update, singleton_local_update.
    { rewrite /to_heap lookup_fmap.
      apply fmap_Some_2.
      rewrite map_filter_lookup_Some Hσv.
        by split.
    }
    by apply exclusive_local_update.
  Qed.

  Lemma heap_write_high σ l v v' :
    surface_val v' →
    heap_ctx σ -∗ l ↦ v ==∗
    heap_ctx (heap_fupd (<[l:=(v')]>) σ) ∗ l ↦ v'.
  Proof.
    iIntros (?) "Hctx Hmt".
    iDestruct (heap_mapsto_high with "Hctx Hmt") as %?.
    iDestruct "Hctx" as (hF sps) "(Hσ & HhF & HI & HhT & Hsps & % & % &#?)".
    rewrite heap_mapsto_eq. iDestruct "Hmt" as "[_ Hmt]".
    iDestruct (heap_mapsto_lookup with "Hσ Hmt") as %?; auto.
    iMod (heap_write_vs_high with "Hσ Hmt") as "[Hσ $]" => //.
    iModIntro. iExists _, _. iFrame. rewrite sp_rel_heap_fupd.
    iSplitR; first by eauto using heap_freeable_rel_stable.
    iSplit => //. iPureIntro. by apply: map_Forall_insert_2.
  Qed.

  (** Weakest Preconditions for Low heap *)
  Lemma heap_alloc_vs_low σ l n :
    low l →
    own heap_name (● to_heap σ)
    ==∗ own heap_name (● to_heap (init_mem l n σ)).
  Proof.
    intros LOW. revert l LOW.
    induction n as [|n IH]=> l LOW; [iIntros; done |] => /=.
    etrans; first apply (IH (l +ₗ 1)). by apply low_shift.
    by rewrite to_heap_insert_low.
  Qed.

  Lemma heap_alloc_low σ l n :
    low l →
    heap_ctx σ ==∗ heap_ctx (init_mem l (Z.to_nat n) σ).
  Proof.
    intros ?; iDestruct 1 as (hF sps) "(Hvalσ & HhF & HI &?&Hsps& % & %& % &#?)".
    iMod (heap_alloc_vs_low _ _ (Z.to_nat n) with "[$Hvalσ]") as "Hvalσ" => //.
    iModIntro. iExists _, _. rewrite sp_rel_init_mem trace_init_mem interface_init_mem interface_surface_init_mem.
    iFrame. do ! iSplitR; eauto using heap_freeable_rel_init_mem_low, heap_surface_init_mem.
  Qed.

  Lemma heap_free_vs_low σ l n :
    low l →
    own heap_name (● to_heap σ)
    ==∗ own heap_name (● to_heap (free_mem l n σ)).
  Proof.
    revert l. induction n as [|n IH]=> l Hl; [iIntros; done|] => /=.
    etrans; first apply (IH (l +ₗ 1)). by apply low_shift.
    by rewrite to_heap_delete_low.
  Qed.

  Lemma heap_free_low σ l (n : Z) :
    low l →
    heap_ctx σ ==∗ heap_ctx (free_mem l (Z.to_nat n) σ).
  Proof.
    iIntros (Hlow).
    iDestruct 1 as (hF sps) "(Hvalσ & HhF & HI & ? & Hsps & REL & % &% & #?)"; iDestruct "REL" as %REL.
    iMod (heap_free_vs_low with "Hvalσ") as "Hvalσ" => //.
    iModIntro. iExists _, _. rewrite sp_rel_free_mem trace_free_mem interface_free_mem interface_surface_free_mem.
    iFrame. do ! iSplitR; eauto using heap_freeable_rel_free_mem_low, heap_surface_free_mem.
  Qed.


  Lemma heap_write_vs_low σ l v:
    low l →
    own heap_name (● to_heap σ)
        ==∗ own heap_name (● to_heap (heap_fupd (<[l:=v]>) σ)).
  Proof. iIntros (Hlow) "Hown". by rewrite to_heap_insert_low. Qed.

  Lemma heap_write_low σ l v :
    low l → surface_val v →
    heap_ctx σ ==∗ heap_ctx (heap_fupd (<[l:=v]>) σ).
  Proof.
    move => ??.
    iDestruct 1 as (hF sps) "(Hσ & HhF & HI & ? & Hsps & % & % &#?)".
    iMod (heap_write_vs_low with "Hσ") as "Hσ"; first done.
    iModIntro. iExists _, _. iFrame.
    iSplitR; first by eauto using heap_freeable_rel_stable_low.
    rewrite sp_rel_heap_fupd. iSplit => //. iPureIntro.
    by apply: map_Forall_insert_2.
  Qed.

End heap.

Section syscall.
  Context `{heapG Σ}.

  Lemma heap_syscall subSP `{inSP Σ subSP} P s v r σ:
    sysaxioms (trace σ ++ [(s, v, r)]) →
    (∀ st (i : nat) π, subSP.(spg_prop) (insp_name subSP heap_spg_name) st -∗
       ∃ idxs, ([∗ list] i∈idxs, ∃ o, in_trace i o) ∗
            (⌜i > max_list_Z idxs⌝ -∗
              in_trace i (s, v, r) -∗
              full_trace π -∗ ⌜subSP.(spg_axioms) π⌝
           ==∗
        ∃ st', ⌜run_sp_obs subSP (s, v, r) st = Some st'⌝ ∗
                subSP.(spg_prop) (insp_name subSP heap_spg_name) st' ∗
                full_trace π ∗
                  P r)) -∗
         heap_ctx σ ==∗ heap_ctx (add_observation (s, v, r) σ) ∗
      P r.
  Proof.
    iIntros (?) "Hstep".
    iDestruct 1 as (hF sps) "(?& ? & ? & HhT & Hsps &? & ? & ? & %)".
    iDestruct (insp_prop_upd with "Hsps") as "[Hsps Hupd]" => /=.
    iDestruct ("Hstep" with "Hsps") as (?) "[Hib Hstep]".
    iDestruct (in_full_trace_idxs with "HhT Hib") as %?.
    iMod (full_trace_add_obs s v r with "HhT") as "[HhT #Hin]".
    iMod ("Hstep" with "[//] Hin HhT []") as "Hstep"; eauto.
    { iPureIntro. by apply/insp_axioms/heap_spg_axioms_equiv. }
    iDestruct "Hstep" as (st') "[% [Hst' [HhT $]]]".
    iModIntro. iExists _, _. iFrame.
    iSplitL "Hst' Hupd"; eauto using sp_rel_add_obs.
      by iApply "Hupd".
  Qed.

End syscall.
