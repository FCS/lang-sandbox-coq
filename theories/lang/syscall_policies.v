From stdpp Require Export gmap.
From iris.program_logic Require Import weakestpre.
From iris.proofmode Require Import tactics.
From sandbox.lang Require Export lang.
From sandbox.lang Require Import notation.
Set Default Proof Using "Type".

(** syscall policies are automata defined using guarded commands *)
Definition transition_fn (state : Type) := base_lit → base_lit → state → option state.
Record syscall_policy := {
   sp_state : Type;
   sp_initial_state : sp_state;
   sp_transitions : gmap string (transition_fn sp_state);
}.

Instance sp_state_inhabited (SP : syscall_policy) : Inhabited (SP.(sp_state)) := populate SP.(sp_initial_state).

Definition run_sp_obs (SP : syscall_policy) (obs : observation) (s : SP.(sp_state)): option SP.(sp_state)
  := f ← (SP.(sp_transitions) !! obs.1.1); f obs.1.2 obs.2 s.

Definition run_sp_trace (SP : syscall_policy) (ts : list observation): option SP.(sp_state) :=
  foldl (λ s obs, s ← s; run_sp_obs SP obs s) (Some SP.(sp_initial_state)) ts.

Lemma run_sp_trace_app SP o os :
  run_sp_trace SP (os ++ [o]) = s ← run_sp_trace SP os; run_sp_obs SP o s.
Proof. by rewrite /run_sp_trace foldl_app. Qed.

Class SP_Disjoint SP1 SP2 := sp_disjoint: (dom (gset string) SP1.(sp_transitions) ## dom (gset string) SP2.(sp_transitions)).

Definition merge_transitions {SP1 SP2} (t1 : option (transition_fn SP1.(sp_state))) (t2 : option (transition_fn SP2.(sp_state))) : option (transition_fn (SP1.(sp_state) * (SP2.(sp_state)))) :=
  (option_map (λ f v t s, (λ s', (s', s.2)) <$> f v t s.1) t1) ∪
  (option_map (λ f v t s, (λ s', (s.1, s')) <$> f v t s.2) t2).
Global Instance merge_trans_DiagNone SP1 SP2 : DiagNone (@merge_transitions SP1 SP2).
Proof. by constructor. Qed.

Definition mergeSP (sp1 : syscall_policy) (sp2 : syscall_policy) := {|
  sp_state := (sp1.(sp_state) * sp2.(sp_state));
  sp_initial_state := (sp1.(sp_initial_state), sp2.(sp_initial_state));
  sp_transitions := merge merge_transitions sp1.(sp_transitions) sp2.(sp_transitions)
|}.

(** iris related stuff *)
Record syscall_policyG Σ := SafetyPropG {
  spg_SP :> syscall_policy;
  spg_name : Type;
  spg_prop : spg_name → spg_SP.(sp_state) → iProp Σ;
  spg_axioms : list observation → Prop;
  spg_init : (|==> ∃ γ, spg_prop γ spg_SP.(sp_initial_state))%I;
}.
Arguments spg_SP {_} _.
Arguments spg_name {_} _.
Arguments spg_prop {_} _.
Arguments spg_axioms {_} _.
Arguments spg_init {_} _.

Class inSP' Σ (subSP : syscall_policyG Σ) (SP : syscall_policyG Σ) := {
  insp_state : SP.(sp_state) → subSP.(sp_state);
  insp_state_upd : subSP.(sp_state) → SP.(sp_state) → SP.(sp_state);
  insp_name : SP.(spg_name) → subSP.(spg_name);
  insp_trans : ∀ obs st st',
    run_sp_obs subSP obs (insp_state st) = Some st' →
    run_sp_obs SP obs st = Some (insp_state_upd st' st);
  insp_prop_upd : ∀ γ st,
      (spg_prop SP γ st -∗
          spg_prop subSP (insp_name γ) (insp_state st) ∗
          (∀ st', spg_prop subSP (insp_name γ) st' -∗
                    spg_prop SP γ (insp_state_upd st' st)))%I;
  insp_axioms : ∀ π, spg_axioms SP π → spg_axioms subSP π;
}.
Arguments insp_state {_} _ {_ _}.
Arguments insp_state_upd {_} _ {_ _}.
Arguments insp_name {_} _ {_ _}.

Definition insp_state_fupd {Σ} subSP {SP} `{inSP' Σ subSP SP} (f : subSP.(sp_state) → subSP.(sp_state)) (s : SP.(sp_state)) : SP.(sp_state) :=
  insp_state_upd subSP (f (insp_state subSP s)) s.

Global Program Instance insp_reflexive Σ SP : inSP' Σ SP SP := {|
  insp_state := id;
  insp_state_upd := const;
  insp_name := id;
|}.
Next Obligation. done. Qed.
Next Obligation.
  move => ???? /=. iIntros "?". iFrame. by iIntros (?) "?".
Qed. Next Obligation. done. Qed.

Program Definition mergeSPG Σ (SP1 : syscall_policyG Σ) (SP2 : syscall_policyG Σ) : syscall_policyG Σ := {|
  spg_SP := mergeSP SP1 SP2;
  spg_name := (SP1.(spg_name) * SP2.(spg_name));
  spg_prop := λ γ sps, (SP1.(spg_prop) γ.1 sps.1 ∗ SP2.(spg_prop) γ.2 sps.2)%I;
  spg_axioms := λ π, SP1.(spg_axioms) π ∧ SP2.(spg_axioms) π;
|}.
Next Obligation.
  move => ? SP1 SP2.
  iMod (spg_init SP1) as (γ1) "H1". iMod (spg_init SP2) as (γ2) "H2".
  iModIntro. iExists (γ1, γ2). by iFrame.
Qed.

Global Program Instance insp_merge_left Σ SP1 SP2 : inSP' Σ SP1 (mergeSPG Σ SP1 SP2) := {|
  insp_state := fst;
  insp_state_upd := λ s1 s, (s1, s.2);
  insp_name := fst;
|}.
Next Obligation.
  rewrite /run_sp_obs => ?????? /=.
  rewrite lookup_merge /merge_transitions => /bind_Some [f [-> Hf]] /=.
  by rewrite option_union_Some_l /= Hf.
Qed.
Next Obligation.
  move => ?????/=. iIntros "[? ?]". iFrame. by iIntros (?) "?".
Qed. Next Obligation. naive_solver. Qed.

Global Program Instance insp_merge_right Σ (SP1 : syscall_policyG Σ) (SP2 : syscall_policyG Σ) `{SP_Disjoint SP1 SP2} : inSP' Σ SP2 (mergeSPG Σ SP1 SP2) := {|
  insp_state := snd;
  insp_state_upd := λ s2 s, (s.1, s2);
  insp_name := snd;
|}.
Next Obligation.
  rewrite /run_sp_obs => ? SP1 ?? obs ?? /=.
  rewrite lookup_merge /merge_transitions =>/bind_Some [f [Hin Hf]] /=.
  have ->: ((sp_transitions SP1 !! obs.1.1) = None). {
    apply (not_elem_of_dom (D:=gset string)) => ?.
    eapply sp_disjoint => //. eapply elem_of_dom. by eexists.
  }
  by rewrite Hin /= Hf.
Qed.
Next Obligation.
  move => ??????/=. iIntros "[? ?]". iFrame. by iIntros (?) "?".
Qed. Next Obligation. naive_solver. Qed.

(** example syscall policies *)
Definition noneSP : syscall_policy := {| sp_state := (); sp_initial_state := (); sp_transitions := ∅ |}.

Definition abortSP : syscall_policy := {|
  sp_state := ();
  sp_initial_state := ();
  sp_transitions := {[ "abort" := (λ _ _ _, None) ]} |}.

Program Definition liftSPG Σ (SP : syscall_policy) : syscall_policyG Σ := {|
  spg_SP := SP;
  spg_name := ();
  spg_prop := λ γ sps, True%I;
  spg_axioms := λ π, True;
|}.
Next Obligation. move => ??. by iExists (). Qed.
