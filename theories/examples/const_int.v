From sandbox.lang Require Import lang notation proofmode type wrapper adversary robust_safety interface unfold.
From sandbox.lang.lib Require Import assert product.

Section const_int.
  Context `{sandboxG Σ}.

  Definition use_2 : val :=
    λ: ["v"], let: "cmp" := "v" = #2 in assert ["cmp"].

  Lemma type_use_2 :
    use_2 ◁ fn High [# const_int 2] any.
  Proof.
    iApply type_fn => //. solve_surface_expr. do 2 iModIntro. iIntros (xl).
    inv_vec xl => v /=. iIntros "[Hv _]". simpl_subst.
    iApply (type_let with "[Hv]"). by iApply (type_eq with "Hv").
    iIntros (cmp) "Hcmp _". case_bool_decide => //=. simpl_subst.
    iApply type_fn_call => //. by iApply type_assert.
    by iSplitL.
  Qed.

  Definition use_2_call : val :=
    λ: [], let: "v" := wrap_import (nroot.@"use_2_call") [] (const_int 2) [] in
           use_2 ["v"].

  Lemma type_use_2_call :
    use_2_call ◁ fn High [# ] any.
  Proof.
    Local Opaque use_2.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl. iIntros "_". simpl_subst.
    iApply type_let. {
      iApply type_fn_call => //. iApply (type_wrap_import _ []) => //=. done.
    }
    iIntros (v) "? _". simpl_subst.
    iApply type_fn_call => //. by iApply type_use_2. by iFrame.
  Qed.

  Local Transparent wrap_import wrap_export import_const_int import_int export_lowptr export_int import_any export_product import_product export_any.
  Definition use_2_export : val := wrap_export use_2 [const_int 2] any.
  Definition use_2_export_val : val.
  Proof using.
    let t:= eval unfold use_2_export, wrap_export in use_2_export in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma use_2_export_val_eq : use_2_export_val = use_2_export.
  Proof. by apply: recv_f_equal. Qed.

  Definition use_2_call_export : val := wrap_export use_2_call [] any.
  Definition use_2_call_export_val : val.
  Proof using.
    let t:= eval unfold use_2_call_export, wrap_export in use_2_call_export in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma use_2_call_export_val_eq : use_2_call_export_val = use_2_call_export.
  Proof. by apply: recv_f_equal. Qed.

End const_int.

Definition interface (u : interface_map) := λ p, match p with
                | Low => u
                | High =>
                  <[ nroot.@ "1" := use_2_export_val]> $
                  <[ nroot.@ "2" := use_2_call_export_val]> ∅
                end.

Lemma use_2_interface_safe u t2 σ2 :
  let _ := SyscallG (λ _, True) in
  map_Forall (λ _ v, surface_val v) u →
  rtc erased_step ([(untrusted_main_val [] at High)%E], initial_state ∅ (interface u)) (t2, σ2) →
  is_good abortSP σ2.
Proof.
  set Σ : gFunctors := #[sandboxΣ]. move=>???.
  eapply (robust_safety Σ _ abortSP ∅) => //.
  move => ?. exists (liftSPG Σ abortSP). split => //. split => //???.
  rewrite untrusted_main_val_eq use_2_export_val_eq use_2_call_export_val_eq.
  iIntros "_ !#". iSplit; last by iApply type_untrusted_main.
  rewrite !big_sepM_insert => //. 2:{
    rewrite lookup_insert_None; split => //.
      by intros [??]%ndot_inj.
  }
  do ! iSplit => //.
  - iExists _. iApply (type_wrap_export _ [const_int 2] _ _ type_use_2) => //=.
  - iExists _. iApply (type_wrap_export _ [] _ _ type_use_2_call) => //=.
Qed.

Print Assumptions use_2_interface_safe.
