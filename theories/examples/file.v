From sandbox.lang Require Import lang notation proofmode type robust_safety.
From sandbox.lang.lib Require Import file assert.

Section file.
  Context `{sandboxG Σ} `{fileG Σ} `{inSP Σ fileSPG}.

  Definition use_file : val :=
    λ: [], let: "f" := do_open [ #1 ] in
              let: "p" := do_read ["f"] in
              let: "f" := ! ("p" +ₗ #0) in
              do_close ["f"].

  Lemma type_use_file:
    use_file ◁ fn High [# ] any.
  Proof.
    Local Opaque do_open do_read do_close.
    iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => /=. iIntros "_". simpl_subst.
    iApply (type_let with "[]"). {
      iApply (type_fn_call $! type_do_open with "[]") => //.
        by iSplit. }
    iIntros (f) "Hf _". simpl_subst.
    iApply (type_let with "[Hf]"). {
      iApply (type_fn_call $! type_do_read with "[Hf]") => //.
       by iFrame. }
    iIntros (f2) "Hf _". simpl_subst.
    iApply (type_let with "[Hf]"). {
      by iApply type_read => //.
    }
    iIntros (?) "Hv _". simpl_subst.
    iApply (type_fn_call $! type_do_close) => //.
    by iFrame.
  Qed.

  Definition use_file_assert : val :=
    λ: [], use_file [ ];; assert [ #1 ].

  Lemma type_use_file_assert:
    use_file_assert ◁ fn High [# ] any.
  Proof.
    Local Opaque use_file.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl => /=. iIntros "_". simpl_subst.
    iApply (type_let with "[]"). {
      by iApply (type_fn_call $! type_use_file with "[]"). }
    iIntros (?) "_ _". simpl_subst.
    iApply (typed_expr_mono any True%I) => //. iIntros (?). by iApply ty_surface.
    iApply (type_fn_call $! (type_assert _)) => //. by iSplitL.
  Qed.
End file.

Definition interface (u : interface_map) := λ p, match p with
                | Low => u
                | High => ∅
                end.

Lemma use_file_safe u t2 σ2 :
  let _ := SyscallG file_axioms in
  map_Forall (λ _ v, surface_val v) u →
  rtc erased_step ([(use_file [] at High)%E], initial_state ∅ (interface u)) (t2, σ2) →
  is_good fileSP σ2.
Proof.
  set Σ : gFunctors := #[sandboxΣ; fileΣ].
  eapply (robust_safety Σ _ _ ∅) => // Hpre.
  exists fileSPG. split_and? => //? HSP HPre.
  iIntros "_ !#". iSplit => //.
  iApply @type_use_file. rewrite HSP HPre. by apply insp_reflexive.
Qed.

Lemma use_file_assert_safe u t2 σ2 :
  let _ := SyscallG file_axioms in
  map_Forall (λ _ v, surface_val v) u →
  rtc erased_step ([(use_file_assert [] at High)%E], initial_state ∅ (interface u)) (t2, σ2) →
  is_good (mergeSP fileSP abortSP) σ2.
Proof.
  set Σ : gFunctors := #[sandboxΣ; fileΣ].
  eapply (robust_safety Σ _ (mergeSP fileSP abortSP) ∅) => // ?.
  exists (mergeSPG Σ fileSPG (liftSPG Σ abortSP)).
  split => //. split => // ?. by rewrite /= right_id. move => HSP HP.
  iIntros "_ !#". iSplit => //.
  iApply @type_use_file_assert. rewrite HSP HP. by apply _.
Qed.

Print Assumptions use_file_safe.
Print Assumptions use_file_assert_safe.
