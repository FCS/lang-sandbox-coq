From sandbox.lang Require Import lang notation proofmode type syscall_policies robust_safety wrapper interface unfold.
From sandbox.lang.lib Require Import file assert sealing product.

Definition seal_name : namespace := nroot .@ "file".
Section sealing.
  (** sandboxG collects the necessary type classes to work with the
  ghost state we need. fileG contains the type classes for the file
  ghost state. inSP Σ fileSPG states that the system call policy
  fileSPG is part of the global system call policy. *)
  Context `{sandboxG Σ} `{fileG Σ} `{inSP Σ fileSPG}.

  (** Since [sealed s ty]  is equivalent to [ty] we can directly use [type_do_open]. *)
  Lemma type_do_open_sealed : do_open ◁ fn High [# int] (sealed seal_name file).
  Proof. iApply type_do_open. Qed.
  Lemma type_do_read_sealed : do_read ◁ fn High [# (sealed seal_name file)] (product [sealed seal_name file; any]).
  Proof. iApply type_do_read. Qed.
  Lemma type_do_close_sealed : do_close ◁ fn High [# (sealed seal_name file)] any.
  Proof. iApply type_do_close. Qed.

  (** This defines the wrapped version of the file functions. *)
  Definition do_open_export : val := wrap_export do_open [# int] (sealed seal_name file).
  Definition do_read_export : val := wrap_export do_read [# (sealed seal_name file)] (product [sealed seal_name file; any]).
  Definition do_close_export : val := wrap_export do_close [# (sealed seal_name file)] any.

  (** We now have the wrapped functions but we cannot use them
  directly in expose_file_safe as they depend on sandboxG, which is
  not available where we want to use these functions in
  expose_file_safe. So we need this hack of defining
  do_open_export_val which is the same as do_open_export as
  do_open_export_val_eq shows, but it does not depend on sandboxG. *)
  Local Transparent wrap_import wrap_export import_const_int import_int export_lowptr export_int import_any export_product import_product export_any export_sealed import_sealed.
  Definition do_open_export_val : val.
  Proof using.
    let t:= eval unfold do_open_export, wrap_export in do_open_export in solve_unfold t.
    Unshelve. apply _.
  Defined.
  Lemma do_open_export_val_eq : do_open_export_val = do_open_export.
  Proof. by apply: recv_f_equal. Qed.

  Definition do_read_export_val : val.
  Proof using.
    let t:= eval unfold do_read_export, wrap_export in do_read_export in solve_unfold t.
    Unshelve. apply _.
  Defined.
  Lemma do_read_export_val_eq : do_read_export_val = do_read_export.
  Proof. by apply: recv_f_equal. Qed.

  Definition do_close_export_val : val.
  Proof using.
    let t:= eval unfold do_close_export, wrap_export in do_close_export in solve_unfold t.
    Unshelve. apply _.
  Defined.
  Lemma do_close_export_val_eq : do_close_export_val = do_close_export.
  Proof. by apply: recv_f_equal. Qed.
End sealing.

(** The interface contains the untrusted code [u] in the Low interface
and the wrapped file functions in the High interface. *)
Definition interface (u : interface_map) := λ p, match p with
                | Low => u
                | High =>
                  <[ nroot.@ "open" := do_open_export_val]> $
                  <[ nroot.@ "read" := do_read_export_val]> $
                  <[ nroot.@ "close" := do_close_export_val]> ∅
                end.

(** The main theorem of this example, which shows that it is safe to
expose the wrapped file functions to arbitrary untrusted code. *)
Theorem expose_file_safe u t2 σ2 :
  (** We assume the kernel respects file_axioms. *)
  let _ := SyscallG file_axioms in
  (** For all untrusted code, represented as arbitrary surface values,  *)
  map_Forall (λ _ v, surface_val v) u →
  (** Each state [σ2] which is reachable from the heap with the global
  variable [seal_name] initialized to an empty map and the interface [interface u]*)
  rtc erased_step ([(untrusted_main_val [] at High)%E], initial_state (<[seal_name := cmap_global_init ]> ∅) (interface u)) (t2, σ2) →
  (** fulfills the fileSP system call policy. *)
  is_good fileSP σ2.
Proof.
  set Σ : gFunctors := #[sandboxΣ; fileΣ]. move=>??.
  eapply (robust_safety Σ) => //. move => ?. exists fileSPG.
  split_and? => //? HSP HPre.
  have Hf : (inSP Σ (@fileSPG _ heap_sandboxPre _)) by rewrite HPre -HSP; apply insp_reflexive.
  rewrite untrusted_main_val_eq do_open_export_val_eq do_read_export_val_eq do_close_export_val_eq.
  rewrite big_sepM_singleton. iIntros "Hl".
  rewrite -HPre.
  iMod (cmap_global_init_l (global_to_loc seal_name) file with "Hl") as "#Hl".
  iModIntro.

  iSplit; last by iApply type_untrusted_main.
  rewrite !big_sepM_insert => //; rewrite ?lookup_insert_None; split_and? => //; try by intros [??]%ndot_inj.


  do ! iSplit => //; iExists _.
  Local Transparent import_sealed export_sealed export_product.
  - iApply (type_wrap_export _ [int] (sealed seal_name file) _ type_do_open_sealed) => //=.
    iModIntro. iSplit => //=.
  - iApply (type_wrap_export _ [(sealed seal_name file)] (product [sealed seal_name file; any]) _ type_do_read_sealed) => //=.
    iModIntro. do ! (iSplit; try iModIntro) => //=.
  - iApply (type_wrap_export _ [(sealed seal_name file)] (any) _ type_do_close_sealed) => //=.
    iModIntro. do ! iSplit => //=.
Qed.

Print Assumptions expose_file_safe.
