From stdpp Require Import gmap.
From sandbox.lang Require Import lang notation proofmode type wrapper adversary robust_safety interface unfold.
From sandbox.lang.lib Require Import assert product.

Definition small_limit : Z := 10.

Definition smallSP : syscall_policy := {|
  sp_state := ();
  sp_initial_state := ();
  sp_transitions :=
    <["small" := λ v' r s, v ← lit_to_Z v';
          if bool_decide (v ≤ small_limit) then Some () else None ]>
    ∅
|}.

Section small.
  Context `{sandboxG Σ} `{inSP Σ (liftSPG Σ smallSP)}.
  Program Definition small_int : type :=
    {| ty_own v := match v return _ with
                   | LitV (LitInt z) => ⌜z ≤ small_limit⌝
                   | _ => False
                   end%I |}.
  Next Obligation. move => [[]|] * //; by iIntros "?". Qed.

  Global Instance small_int_copy : Copy small_int.
  Proof.
    case; last by apply _. by case; apply _.
  Qed.

  Global Instance import_small_int : Import small_int :=
    {| import_ctx := True%I;
       import_fn := (λ: ["x"], let: "v" := import_fn int ["x"] in
                               if: "v" ≤ #small_limit then "v" else stuck_expr
                               ) |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply (type_let).
    { iApply type_fn_call => //.  by iApply import_type. by iSplit. }
    iIntros (v) "#Hv _". simpl_subst.
    iDestruct (int_to_const_int with "Hv") as (m) "Hm".
    iApply type_case. by iApply type_leq.
    iIntros (i er ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]].
    - by iApply type_stuck.
    - iApply type_val.
      iDestruct (const_int_inv with "H1") as %->.
      iDestruct (const_int_inv with "H2") as %Heq.
      iDestruct (const_int_inv with "Hm") as %->.
      move: Heq. by case_bool_decide.
  Defined.
  Global Opaque import_small_int.

  Lemma small_int_inv v : v ◁ small_int -∗ ⌜∃ (z : Z), v = #z ∧ z ≤ small_limit⌝.
  Proof.
    iIntros "He". case v => // [[| |?]] //.
    iRevert "He". iIntros (?). eauto.
  Qed.

  Definition use_small : val :=
    λ: ["v"], Syscall "small" "v".

  Lemma type_use_small :
    use_small ◁ fn High [# small_int] any.
  Proof.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl => v /=. iIntros "[Hv _]". simpl_subst.
    iDestruct (small_int_inv with "Hv") as %[z [-> ?]].
    iApply (type_syscall _ _ True%I) => //.
    iIntros (? ? ? ?) "_". iExists []. iSplit => //.
    iIntros (?) "_ ? _". iExists (). iModIntro. iFrame. iSplit => //.
    rewrite /run_sp_obs /=. simpl_map => /=.
      by case_bool_decide.
  Qed.

  Definition use_small_call : val :=
    λ: [], let: "v" := wrap_import (nroot.@"use_small") [] small_int [] in
           use_small ["v"].

  Lemma type_use_small_call :
    use_small_call ◁ fn High [# ] any.
  Proof.
    Local Opaque use_small.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl. iIntros "_". simpl_subst.
    iApply type_let. {
      iApply type_fn_call => //. iApply (type_wrap_import _ []) => //=. done.
    }
    iIntros (v) "? _". simpl_subst.
    iApply type_fn_call => //. by iApply type_use_small. by iFrame.
  Qed.

  Local Transparent wrap_import wrap_export import_small_int import_int export_lowptr export_int import_any export_any export_product import_product export_any.
  Definition use_small_export : val := wrap_export use_small [small_int] any.
  Definition use_small_export_val : val.
  Proof using.
    let t:= eval unfold use_small_export, wrap_export in use_small_export in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma use_small_export_val_eq : use_small_export_val = use_small_export.
  Proof. by apply: recv_f_equal. Qed.

  Definition use_small_call_export : val := wrap_export use_small_call [] any.
  Definition use_small_call_export_val : val.
  Proof using.
    let t:= eval unfold use_small_call_export, wrap_export in use_small_call_export in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma use_small_call_export_val_eq : use_small_call_export_val = use_small_call_export.
  Proof. by apply: recv_f_equal. Qed.

End small.

Definition interface (u : interface_map) := λ p, match p with
                | Low => u
                | High =>
                  <[ nroot.@ "1" := use_small_export_val]> $
                  <[ nroot.@ "2" := use_small_call_export_val]> $
                  ∅
                end.

Lemma use_small_safe u t2 σ2 :
  let _ := SyscallG (λ _, True) in
  map_Forall (λ _ v, surface_val v) u →
  rtc erased_step ([(untrusted_main_val [] at High)%E], initial_state ∅ (interface u)) (t2, σ2) →
  is_good smallSP σ2.
Proof.
  set Σ : gFunctors := #[sandboxΣ]. move=>???.
  eapply (robust_safety Σ _ smallSP ∅) => //.
  move => ?. exists (liftSPG Σ smallSP). split => //. split => //? HSP HPre.
  have Hf : (inSP Σ (liftSPG Σ smallSP)) by rewrite -HSP; apply insp_reflexive.
  rewrite untrusted_main_val_eq use_small_export_val_eq use_small_call_export_val_eq.
  iIntros "_ !#". iSplit; last by iApply type_untrusted_main.
  rewrite !big_sepM_insert => //. 2:{
    rewrite lookup_insert_None; split => //.
      by intros [??]%ndot_inj.
  }
  do ! iSplit => //.
  - iExists _. iApply (type_wrap_export _ [small_int] _ _ type_use_small) => //=.
  - iExists _. iApply (type_wrap_export _ [] _ _ type_use_small_call) => //=.
Qed.

Print Assumptions use_small_safe.
