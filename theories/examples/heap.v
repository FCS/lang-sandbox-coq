From sandbox.lang Require Import lang notation proofmode.

Section examples.
  Context `{sandboxG Σ}.
  Definition assert_int : val :=
    λ: ["x"], "x" + #1;; #☠.

  Lemma assert_int_wp p E v:
    {{{ True }}}
      assert_int [ v ] at p @ E ?
    {{{(n : Z), RET #☠ at p; ⌜v = #n⌝ }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_rec.
    wp_bind (BinOp _ _ _).
    move: v => [|*]; last by solve_stuck.
    move => [|?|n]; [solve_stuck..|].
    wp_op. wp_seq.
    by iApply "HΦ".
  Qed.

  Definition read_write_high : expr :=
    let: "l" := Alloc High #1 in
    "l" <- #0;;
    let: "v" := !"l" in
    Free #1 "l";;
    "v".

  Example read_write_high_wp :
    {{{ True }}}
      read_write_high at High ?
    {{{ RET #0 at High; True }}}.
  Proof.
    rewrite /read_write_high. iIntros (Φ) "_ HΦ".
    wp_alloc_high l as "Hl" "Hlfree". rewrite heap_mapsto_vec_singleton.
    wp_let.
    wp_write_high.
    wp_read_high.
    wp_let.
    rewrite -heap_mapsto_vec_singleton.
    wp_free_high => //.
    by iApply "HΦ".
  Qed.

  Definition read_write_low : expr :=
    let: "l" := Alloc Low #1 in
    "l" <- #0;;
    let: "v" := !"l" in
    "l" <- "v";;
    assert_int [ "v" ];;
    withcont: "end" :
      if: "v" = #0 then "end" [ "v" ] else "end" [ #0 ]
    cont: "end" ["v"] :=
    Free #1 "l";;
    "v".

  Example read_write_low_wp :
    {{{ True }}}
      read_write_low at Low
    ? {{{ RET #0 at Low; True }}}.
  Proof.
    rewrite /read_write_low. iIntros (Φ) "_ HΦ".
    wp_alloc_low l => Hl.
    wp_let.
    wp_write_low.
    wp_read_low v => ?. wp_let.
    wp_write_low.
    wp_apply assert_int_wp => //. iIntros (n ->). wp_seq.
    wp_rec.
    wp_op.
    case_bool_decide as Hn; wp_case;
      wp_rec; wp_free_low; rewrite ? Hn;
        by iApply "HΦ".
  Qed.
End examples.
